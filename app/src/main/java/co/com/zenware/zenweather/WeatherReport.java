package co.com.zenware.zenweather;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import co.com.zenware.zenweather.rest.ilmatee.Forecast;
import co.com.zenware.zenweather.rest.ilmatee.Place;
import co.com.zenware.zenweather.rest.ilmatee.Wind;

/**
 * Created by John on 4/10/2015.
 */
public class WeatherReport {

    MyListAdapter myListAdapter;
    public Forecast forecast;
    public WeatherReport(MyListAdapter myListAdapter){
        this.myListAdapter=myListAdapter;
    }


    public void setForecast(Forecast forecast,MainActivity.CYCLE DAY_NIGHT) {
        this.forecast = forecast;
        List<String> new_header =new ArrayList<>();
        new_header.add("Forecast");
        new_header.add("Sea");
        new_header.add("Peipsi");
        if(DAY_NIGHT.equals(MainActivity.CYCLE.NIGHT)){
            myListAdapter.get_listDataChild().put("Forecast", new ArrayList<String>(Arrays.asList(forecast.night.text)));
            myListAdapter.get_listDataChild().put("Sea", new ArrayList<String>(Arrays.asList(forecast.night.sea)));
            myListAdapter.get_listDataChild().put("Peipsi", new ArrayList<String>(Arrays.asList(forecast.night.peipsi)));
        }
        else if(DAY_NIGHT.equals(MainActivity.CYCLE.DAY)){
            myListAdapter.get_listDataChild().put("Forecast", new ArrayList<String>(Arrays.asList(forecast.day.text)));
            myListAdapter.get_listDataChild().put("Sea", new ArrayList<String>(Arrays.asList(forecast.day.sea)));
            myListAdapter.get_listDataChild().put("Peipsi", new ArrayList<String>(Arrays.asList(forecast.day.peipsi)));
        }
        myListAdapter.set_listDataHeader(new_header);
    }

    public void setForecastPlaces(Forecast forecast,MainActivity.CYCLE DAY_NIGHT) {
        this.forecast = forecast;
        List<String> new_header =new ArrayList<>();
        myListAdapter.set_listDataChild(new HashMap<String, List<String>>());
        if(DAY_NIGHT.equals(MainActivity.CYCLE.NIGHT)){
            for(Place place: this.forecast.night.places){
            new_header.add(place.name);
            myListAdapter.get_listDataChild().put(place.name,new ArrayList<String>(Arrays.asList(place.phenomenon+", Min: "+place.getTempmin()+"ºC Max:"+place.getTempmax()+"ºC")));
            }
            myListAdapter.set_listDataHeader(new_header);
           }
        else if(DAY_NIGHT.equals(MainActivity.CYCLE.DAY)){
            for(Place place: this.forecast.day.place){
                new_header.add(place.name);
                myListAdapter.get_listDataChild().put(place.name,new ArrayList<String>(Arrays.asList(place.phenomenon+", Min: "+place.getTempmin()+"ºC Max:"+place.getTempmax()+"ºC")));
            }
            myListAdapter.set_listDataHeader(new_header);
        }

    }

    public void setForecastWind(Forecast forecast,MainActivity.CYCLE DAY_NIGHT) {
        this.forecast = forecast;
        List<String> new_header =new ArrayList<>();
        myListAdapter.set_listDataChild(new HashMap<String, List<String>>());


        if(DAY_NIGHT.equals(MainActivity.CYCLE.NIGHT)){
            for(Wind wind: this.forecast.night.winds){
                new_header.add(wind.name);
                myListAdapter.get_listDataChild().put(wind.name,new ArrayList<String>(Arrays.asList("MinSpeed:"+wind.getSpeedmin()+", MaxSpeed: "+wind.getSpeedmax()+"Gust:"+wind.getGust()+" WindDir:"+wind.getDirection())));
            }
            myListAdapter.set_listDataHeader(new_header);
        }
        else if(DAY_NIGHT.equals(MainActivity.CYCLE.DAY)){
            for(Wind wind: this.forecast.day.wind){
                new_header.add(wind.name);
                myListAdapter.get_listDataChild().put(wind.name,new ArrayList<String>(Arrays.asList("MinSpeed:"+wind.getSpeedmin()+", MaxSpeed: "+wind.getSpeedmax()+"Gust:"+wind.getGust()+" WindDir:"+wind.getDirection())));
            }
            myListAdapter.set_listDataHeader(new_header);
        }


    }

    public Forecast getForecast() {
        return forecast;
    }

}
