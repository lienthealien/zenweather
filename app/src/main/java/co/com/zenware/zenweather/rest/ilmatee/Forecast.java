package co.com.zenware.zenweather.rest.ilmatee;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.util.Date;

/**
 * Created by John on 4/7/2015.
 */
@Root(name="forecast")
public class Forecast {


    @Attribute(name = "date")
    public String date;
    @Element(name = "night")
    public Night night;
    @Element(name = "day")
    public  Day day;



}
