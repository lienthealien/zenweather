package co.com.zenware.zenweather;

import java.net.MalformedURLException;


import co.com.zenware.zenweather.rest.ilmatee.Forecast;
import co.com.zenware.zenweather.rest.ilmatee.Forecasts;
import retrofit.RestAdapter;
//import retrofit.converter.SimpleXMLConverter;
import retrofit.converter.SimpleXMLConverter;
import retrofit.http.GET;

/**
 * Created by John on 4/7/2015.
 */
public class SimpleClient {
    private static final String API_URL = "http://www.ilmateenistus.ee/ilma_andmed";

    interface WeatherClient {
      @GET("/xml/forecast.php")
      Forecasts listForecasts();
        }

    public static void main(String... args) throws MalformedURLException {
      System.out.println("HI WTF");
      RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(API_URL)
                .setConverter(new SimpleXMLConverter())
                .build();
        WeatherClient weather = restAdapter.create(WeatherClient.class);
        System.out.println(weather);
        Forecasts forecasts=weather.listForecasts();
        for(Forecast forecast:forecasts.forecastList){
            System.out.println(forecast.date);
        }
    }
}
