package co.com.zenware.zenweather.rest.ilmatee;

import retrofit.http.GET;

/**
 * Created by John on 4/7/2015.
 */
public abstract interface WeatherClient {
    @GET("/xml/forecast.php")
    Forecasts listForecasts();
}
