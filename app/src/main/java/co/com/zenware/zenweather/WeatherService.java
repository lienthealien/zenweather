package co.com.zenware.zenweather;

import co.com.zenware.zenweather.rest.ilmatee.Forecast;
import co.com.zenware.zenweather.rest.ilmatee.Forecasts;
import co.com.zenware.zenweather.rest.ilmatee.WeatherClient;
import co.com.zenware.zenweather.service.SimpleClient;
import retrofit.http.GET;

/**
 * Created by John on 4/7/2015.
 */
public class WeatherService extends SimpleClient implements WeatherClient{

    public WeatherService(String url){super(url); }
    @Override
    public Forecasts listForecasts() {
        WeatherClient  weather= init().create(WeatherClient.class);
        Forecasts forecasts=null;
    try{ forecasts=weather!=null?weather.listForecasts():null;}
    catch (Exception e){e.printStackTrace();}
        return forecasts;
    }
}
