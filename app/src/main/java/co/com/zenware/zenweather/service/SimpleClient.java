package co.com.zenware.zenweather.service;

import java.io.Console;
import java.net.MalformedURLException;


import co.com.zenware.zenweather.rest.ilmatee.Forecast;
import co.com.zenware.zenweather.rest.ilmatee.Forecasts;
import co.com.zenware.zenweather.rest.ilmatee.WeatherClient;
import retrofit.RestAdapter;
//import retrofit.converter.SimpleXMLConverter;
import retrofit.converter.SimpleXMLConverter;
import retrofit.http.GET;

/**
 * Created by John on 4/7/2015.
 */
public class SimpleClient   {
    private static String API_URL = "http://www.ilmateenistus.ee/ilma_andmed";
    public SimpleClient(String url){
        this.API_URL=url;
    }

    public RestAdapter init(){
        try{
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(API_URL)
                    .setConverter(new SimpleXMLConverter())
                    .build();
            return restAdapter;

        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }


}
