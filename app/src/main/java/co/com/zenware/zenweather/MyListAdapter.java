package co.com.zenware.zenweather;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import co.com.zenware.zenweather.rest.ilmatee.Forecast;

/**
 * Created by John on 4/9/2015.
 */
public class MyListAdapter extends BaseExpandableListAdapter {
    private Context _context;

    private List<String> _listDataHeader; // header titles
    private HashMap<String, List<String>> _listDataChild;

    public MyListAdapter(Context context) {
      _listDataHeader= new ArrayList<>();Arrays.asList("Text", "Sea", "Peipsi");
        _listDataHeader.add("Text");
        _listDataHeader.add("Sea");
        _listDataHeader.add( "Peipsi");
      _listDataChild=new HashMap<>();
      ArrayList<String> items=  new ArrayList<String>();
      items.add("No Data avaliable");
      _listDataChild.put("Text",items);
      _listDataChild.put("Sea",items);
      _listDataChild.put("Peipsi",items);
      this._context = context;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.lblListItem);

        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }





    public List<String> get_listDataHeader() {
        return _listDataHeader;
    }

    public void set_listDataHeader(List<String> _listDataHeader) {
        this._listDataHeader = _listDataHeader;
    }

    public HashMap<String, List<String>> get_listDataChild() {
        return _listDataChild;
    }

    public void set_listDataChild(HashMap<String, List<String>> _listDataChild) {
        this._listDataChild = _listDataChild;
    }
}
