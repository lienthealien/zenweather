package co.com.zenware.zenweather.rest.ilmatee;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by John on 4/7/2015.
 */
@Root(name="place")
public class Place {

    @Element(name = "name",required = false)
    public String name;
    @Element(name = "phenomenon",required = false)
    public String phenomenon;
    @Element(name = "tempmin",required = false)
    public String tempmin;
    @Element(name = "tempmax",required = false)
    public  String tempmax;

    public String getTempmin() {
        return tempmin==null?"--":tempmin;
    }

    public String getTempmax() {
        return tempmax==null?"--":tempmax;
    }
}
