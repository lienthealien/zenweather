package co.com.zenware.zenweather.rest.ilmatee;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by John on 4/7/2015.
 */
@Root(name = "forecasts")

public class Forecasts {


    @ElementList(inline = true,empty=false)
    public List<Forecast> forecastList;



}


