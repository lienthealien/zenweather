package co.com.zenware.zenweather.rest.ilmatee;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.List;

/**
 * Created by John on 4/7/2015.
 */
public class Day {
    @Element(name = "phenomenon" )
   public String phenomenon;
    @Element(name = "tempmin")
    public String tempmin;
    @Element(name = "tempmax")
    public String tempmax;
    @Element(name = "text")
    public String text;
    @ElementList(inline = true,required = false)
    public List<Place> place;
    @ElementList(inline = true,required = false)
    public List<Wind> wind;
    @Element(name = "sea",required = false)
    public  String sea;
    @Element(name = "peipsi",required = false)
    public String peipsi;

    public String getTempmin() {
        return tempmin==null?"--":tempmin;
    }

    public String getTempmax() {
        return tempmax==null?"--":tempmax;
    }
}
