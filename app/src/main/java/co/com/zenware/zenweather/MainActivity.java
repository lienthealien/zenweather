package co.com.zenware.zenweather;


import android.net.Uri;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.TabHost;
import android.widget.TextView;

import java.util.List;

import co.com.zenware.zenweather.rest.ilmatee.Forecast;
import co.com.zenware.zenweather.rest.ilmatee.Forecasts;
import logic.SuperInteger;


public class MainActivity extends ActionBarActivity implements View.OnClickListener {
    WeatherService weather;
    int MAX_COUNT;
    List<Forecast>foreLst;
    Forecast current_forecast;
    private int COUNT;
    public static enum CYCLE{DAY,NIGHT}
    CYCLE DAY_NIGHT;
    ExpandableListView explv_forecast;
    MyListAdapter mylist_adapter;
    WeatherReport weatherReport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btn_nextday).setOnClickListener(this);
        findViewById(R.id.btn_prevday).setOnClickListener(this);
        findViewById(R.id.imb_btnday).setOnClickListener(this);
        findViewById(R.id.btn_wind).setOnClickListener(this);
        findViewById(R.id.btn_place).setOnClickListener(this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        explv_forecast= (ExpandableListView) findViewById(R.id.explv_forecast);
        mylist_adapter=new MyListAdapter(this );
        explv_forecast.setAdapter(mylist_adapter);
        weatherReport=new WeatherReport(mylist_adapter);
        new LongRunningGetIO().execute();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClick(View v){
     //   Button get_weather_data  ;
        switch(v.getId()) {
            case R.id.btn_prevday:
                if(COUNT-1>=0){COUNT=COUNT-1;
                 current_forecast=foreLst.get(COUNT);
                 ((TextView)findViewById(R.id.txt_date)).setText(current_forecast.date);setFieldsByDate(DAY_NIGHT);

                }
                break;
            case R.id.btn_nextday:
                if(COUNT+1<MAX_COUNT){COUNT=COUNT+1;
                 current_forecast=foreLst.get(COUNT);
                 ((TextView)findViewById(R.id.txt_date)).setText(current_forecast.date);
                  setFieldsByDate(DAY_NIGHT); mylist_adapter.notifyDataSetChanged();}
                break;
            case R.id.imb_btnday:
                setFieldsByDate(DAY_NIGHT);
                mylist_adapter.notifyDataSetChanged();
                break;
            case R.id.btn_place:
                if(DAY_NIGHT.equals(CYCLE.NIGHT)&&current_forecast.night.places!=null&&this.current_forecast.night.places.size()>0 ){
                    weatherReport.setForecastPlaces(current_forecast, DAY_NIGHT);
                    mylist_adapter.notifyDataSetChanged();
                }
                else if(DAY_NIGHT.equals(CYCLE.DAY)&&current_forecast.night.places!=null&&this.current_forecast.night.places.size()>0 ){
                    weatherReport.setForecastPlaces(current_forecast, DAY_NIGHT);
                    mylist_adapter.notifyDataSetChanged();
                }
                break;
            case R.id.btn_wind:
                if(DAY_NIGHT.equals(CYCLE.NIGHT)&&current_forecast.day.wind!=null&&this.current_forecast.day.wind.size()>0 ){
                    weatherReport.setForecastWind(current_forecast, DAY_NIGHT);
                    mylist_adapter.notifyDataSetChanged();
                }
                else if(DAY_NIGHT.equals(CYCLE.DAY)&&current_forecast.day.wind!=null&&this.current_forecast.day.wind.size()>0 ){
                    weatherReport.setForecastWind(current_forecast, DAY_NIGHT);
                    mylist_adapter.notifyDataSetChanged();
                }
                break;
        }
    }

    public void tempToWords(String min, String max){
        SuperInteger maxTemp_word;
        SuperInteger minTemp_word;
        int _min;
        int _max;
        String temp_to_words="temp between ";
        if(!min.equals("--")){
            try{
                _min=Integer.parseInt(min);
                minTemp_word=new SuperInteger(Math.abs(_min));
                temp_to_words+=_min>=0?minTemp_word.toText():" minus "+minTemp_word.toText();
                temp_to_words+=" degrees and ";

            }
            catch (NumberFormatException e){}

        }
        if(!max.equals("--")){
            try{
                _max=Integer.parseInt(max);
                maxTemp_word=new SuperInteger(Math.abs(_max));
                temp_to_words+=_max>=0?maxTemp_word.toText():" minus "+maxTemp_word.toText();
                temp_to_words+=" degrees";
            }
            catch (NumberFormatException e){}
        }
        ((TextView)findViewById(R.id.txt_tempword)).setText(temp_to_words);
    }

    public void setFieldsByDate(CYCLE type) {
        ImageButton btn_day =(ImageButton)findViewById(R.id.imb_btnday);
        if(type.equals(CYCLE.NIGHT)){
            ((TextView)findViewById(R.id.txt_breport)).setText(""+current_forecast.night.phenomenon);
            ((TextView)findViewById(R.id.txt_tempMax)).setText("TempMax:"+current_forecast.night.getTempmax()+"ºC");
            ((TextView)findViewById(R.id.txt_tempMin)).setText("TempMin:"+current_forecast.night.getTempmin()+"ºC");
            weatherReport.setForecast(current_forecast,type);
            mylist_adapter.notifyDataSetChanged();
            btn_day.setImageResource(R.drawable.moon1);
            DAY_NIGHT=CYCLE.DAY;
            tempToWords(current_forecast.night.getTempmin(),current_forecast.night.getTempmax());

        }
        else if(type.equals(CYCLE.DAY)){
            ((TextView)findViewById(R.id.txt_breport)).setText(""+current_forecast.day.phenomenon);
            ((TextView)findViewById(R.id.txt_tempMax)).setText("TempMax:"+current_forecast.day.getTempmax()+"ºC");
            ((TextView)findViewById(R.id.txt_tempMin)).setText("TempMin:"+current_forecast.day.getTempmin()+"ºC");
            weatherReport.setForecast(current_forecast, type);
            mylist_adapter.notifyDataSetChanged();
          //  ((TextView)findViewById(R.id.txt_text)).setText(current_forecast.day.text);
            btn_day.setImageResource(R.drawable.sun1);
            DAY_NIGHT=CYCLE.NIGHT;
            tempToWords(current_forecast.day.getTempmin(),current_forecast.day.getTempmax());
        }

    }

    private class LongRunningGetIO extends AsyncTask<Void, Void, WeatherService> {
        @Override
        protected WeatherService doInBackground(Void... params) {
            try{
                weather=new WeatherService("http://www.ilmateenistus.ee/ilma_andmed");
                return weather;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
        protected void onPostExecute(WeatherService results) {
           Button get_weather_data =(Button)findViewById(R.id.btn_nextday);
            get_weather_data.setClickable(true);
             get_weather_data =(Button)findViewById(R.id.btn_prevday);
            get_weather_data.setClickable(true);
            if (results!=null) {
                foreLst = weather.listForecasts().forecastList;
                MAX_COUNT=foreLst.size();
                current_forecast=foreLst.get(0);
                DAY_NIGHT=CYCLE.DAY;

                ((TextView)findViewById(R.id.txt_date)).setText(current_forecast.date);
                setFieldsByDate(DAY_NIGHT);
            }

        }
    }
}
