package co.com.zenware.zenweather.rest.ilmatee;


import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by John on 4/7/2015.
 */
//@Root(name="wind")
public class Wind {

    @Element(name = "name",required = false)
    public String name;
    @Element(name = "direction",required = false)
    public  String direction;
    @Element(name = "speedmin",required = false)
    public  String speedmin;
    @Element(name = "speedmax",required = false)
    public String speedmax;
    @Element(name = "gust",required = false)
    public  String gust;

    public String getDirection() {
        return direction==null?"--":direction;
    }

    public String getSpeedmin() {
        return speedmin==null?"--":speedmin;
    }

    public String getSpeedmax() {
        return speedmax==null?"--":speedmax;
    }

    public String getGust() {
        return gust==null?"--":gust;
    }
}
